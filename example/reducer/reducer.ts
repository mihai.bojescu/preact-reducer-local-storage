import { Reducer } from 'preact/hooks'
import { Actions, State } from './types'

export const initialState: State = {
  value: 0
}

export const reducer: Reducer<State, Actions> = (state, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return {
        ...state,
        data: state.value + 1
      }
    case 'DECREMENT':
      return {
        ...state,
        data: state.value - 1
      }
    default:
      return state
  }
}
